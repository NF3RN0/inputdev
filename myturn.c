#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <linux/input.h>
#include <math.h>


#define EVENT_DEVICE    "/dev/input/touchscreen"
#define EVENT_TYPE      EV_ABS
#define EVENT_CODE_X    ABS_X
#define EVENT_CODE_Y    ABS_Y
#define SCREEN_WIDTH    320
#define SCREEN_HEIGHT   240
#define MAGIC_NUMBER    65535



/* TODO: Close fd on SIGINT (Ctrl-C), if it's open */

int main(int argc, char *argv[])
{
    struct input_event ev;
    int fd;
    float cast_x;
    float cast_y;
    int final_x;
    int final_y;
    int screen_h = SCREEN_HEIGHT;
    int screen_w = SCREEN_WIDTH;
    int magic_number = MAGIC_NUMBER;
    char name[256] = "Unknown";


    if ((getuid ()) != 0) {
        fprintf(stderr, "You are not root! This may not work...\n");
        return EXIT_SUCCESS;
    }

    /* Open Device */
    fd = open(EVENT_DEVICE, O_RDONLY);
    if (fd == -1) {
        fprintf(stderr, "%s is not a vaild device\n", EVENT_DEVICE);
        return EXIT_FAILURE;
    }

    /* Print Device Name */
    ioctl(fd, EVIOCGNAME(sizeof(name)), name);
    printf("Reading from:\n");
    printf("device file = %s\n", EVENT_DEVICE);
    printf("device name = %s\n", name);

    for (;;) {
        const size_t ev_size = sizeof(struct input_event);
        ssize_t size;

        /* TODO: use select() */

        size = read(fd, &ev, ev_size);
        if (size < ev_size) {
            fprintf(stderr, "Error size when reading\n");
            goto err;
        }

        if (ev.type == EVENT_TYPE && (ev.code == EVENT_CODE_X
                      || ev.code == EVENT_CODE_Y)) {
           
	    
            if (ev.code == EVENT_CODE_X) {
                printf("value is %d\n",ev.value);
                cast_x =  ( (float)ev.value / (float)magic_number) * (float)screen_w;
                final_x = round(cast_x);
                printf("X = %d\n",final_x);

            }
            else {
                 printf("value is %d\n",ev.value);
                 cast_y = ( (float)ev.value / (float)magic_number) * (float)screen_w;
                 final_y = round(cast_y);
                 printf("Y = %d\n", final_y);
            }
        }
    }

    return EXIT_SUCCESS;

err:
    close(fd);
    return EXIT_FAILURE;
}
